import './App.css';
import {HashRouter, Route, Routes} from "react-router-dom";
import Home from './components/HomePage/Home/Home';
import Memory from './components/MemoryMode/Memory/Memory';
import Trivia from 'components/TriviaMode/Trivia/Trivia';
import Navigation from "./components/NavigationComponent/Navigation/Navigation";
import 'bootstrap/dist/css/bootstrap.min.css';
import 'resources/styles/sass/global-style.scss';
import {useChampionData} from "./components/HomePage/ChampionDataList/useChampionData";
import {ChampionDataContextProvider} from "./contexts/ChampionDataContext";
import {LoginProvider} from "./contexts/LoginProvider";
import {Navigate} from "react-router";
import Profile from "./components/ProfilePage/Profile/Profile";


function App() {
    const {championDataMap, loading, error, championNames} = useChampionData();
    return (
        <div>
            <ChampionDataContextProvider value={{championDataMap, loading, error, championNames}}>
                <LoginProvider>
                    <HashRouter>
                        <Navigation className={"App-header"}/>
                        <div className="content-container">
                            <Routes>
                                <Route path="/" element={<Navigate to="/home" />} />
                                <Route path="/home" element={<Home/>}/>
                                <Route path="/memory" element={<Memory/>}/>
                                <Route path="/profile" element={<Profile/>}/>
                                <Route path="/trivia" element={<Trivia/>}/>
                            </Routes>
                            <div className="footer-container" />
                        </div>
                    </HashRouter>
                </LoginProvider>
            </ChampionDataContextProvider>
        </div>
    );
}

export default App;
