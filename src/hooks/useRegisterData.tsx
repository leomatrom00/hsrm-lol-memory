import { useState} from "react";
import {STRAPI_URL} from "./Constants";

const API_REGISTER_URL = STRAPI_URL + "/api/auth/local/register";

interface User {
    username: string,
    password: string,
    name: string,
    email: string,
}

export const useRegisterData = () => {
    const [success, setSuccess] = useState(false);

    const register = async (user: User) => {
        setSuccess(false);

        return fetch(API_REGISTER_URL, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify(user),
        })
        .then((res) => {
            if(!res.ok) throw new Error(res.status.toString());
            return res.json()
        })
        .then((data) => {
            setSuccess(true);
            return null;
        })
        .catch((e: Error) => {
            if(e.message === "400") {
                return "Nutzer oder E-Mail bereits vorhanden.";
            } else {
                return e.message;
            }
        });
    }

    return ({ register, success });
}

