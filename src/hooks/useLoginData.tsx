import { useState } from "react";
import { useLogin } from "../contexts/LoginProvider";
import {STRAPI_URL} from "./Constants";

const API_AUTH_URL = STRAPI_URL + "/api/auth/local";

export const useLoginData = () => {
    const { login } = useLogin();
    const [feedback, setFeedback] = useState<string>("");

    const authorize = async (username: string, password: string): Promise<boolean> => {
        setFeedback("");

        return fetch(API_AUTH_URL, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify({
                identifier: username,
                password: password,
            }),
        })
        .then((res) => {
            if (!res.ok) throw new Error(res.status.toString());
            return res.json()
        })
        .then((data) => {
            sessionStorage.setItem("apiToken", data.jwt);
            login(data.user);
            setFeedback("");
            return true;
        })
        .catch((e: Error) => {
            if(e.message === "400") {
                setFeedback("Nutzername oder Passwort falsch.")
            } else {
                setFeedback(e.message);
            }
            return false;
        });
    }

    return ({ authorize, feedback });
}

