import { useState, useEffect } from 'react';
import { DIFFICULTIES } from 'components/MemoryMode/MemoryBoard/Difficulty';
import { IMAGE_URL_PREFIX } from "./Constants"
import { ChampionInfo } from 'components/ChampionData/ChampionData';

interface MemoryProps {
    championDataMap: Map<String, ChampionInfo | undefined>;
    championNames: string[];
}

export const useMemorySessionState = ({ championDataMap, championNames = [] }: MemoryProps) => {
    const [difficulty, setDifficulty] = useState<DIFFICULTIES>();
    const [memoryCardData, setMemoryCardData] = useState<string[][]>([]);

    useEffect(() => {
        const imageData = collectChampionImages(championDataMap, championNames);
        if (imageData) {
            setMemoryCardData(imageData);
        } // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    function changeDifficulty(newValue: DIFFICULTIES) {
        setDifficulty(newValue);
    }

    return {
        memoryCardData: memoryCardData,
        difficulty: difficulty,
        setDifficulty: changeDifficulty,
    }
};

export function collectChampionImages(championDataMap: Map<String, ChampionInfo | undefined>, championNames: string[]): string[][] | null {
    try {
        return championNames.map(name => {
            return [IMAGE_URL_PREFIX + championDataMap.get(name)?.id + ".png", name];
        });
    } catch (e) {
        console.log(e);
    }
    return null;
}
