import { useLogin } from "../contexts/LoginProvider";
import {STRAPI_URL} from "./Constants";

const API_USER_URL = STRAPI_URL + "/api/users/";

export const useSaveScore = () => {
    const { userData } = useLogin();

    const compareScore = async (score: number, mode: string): Promise<string | void> => {
        const apiKey = sessionStorage.getItem("apiToken");
        const user = await fetch(API_USER_URL + "me", {
            method: "GET",
            headers: {
                Authorization: `Bearer ${apiKey}`,
                "Content-Type": "application/json",
            }}
            ).then((result) => {
                if(!result.ok) throw Error("No user found.");
                return result.json();
        }).catch(() => {
            return "Fetch Fehlgeschlagen.";
        });

        if(!user.username) {
            return user;
        }

        let check;
        let bodyPutContent;
        if (mode === "memory") {
            check = user.memoryHighScore < score;
            bodyPutContent = JSON.stringify({memoryHighScore: score});
        } else {
            check = user.triviaHighScore < score;
            bodyPutContent = JSON.stringify({triviaHighScore: score});
        }
        
        if(check) {
            return fetch(API_USER_URL + userData.id, {
                    method: "PUT",
                    headers: {
                        Authorization: `Bearer ${apiKey}`,
                        "Content-Type": "application/json",
                    },
                    body: bodyPutContent,
                }
            ).then((result) => {
                if (!result.ok) throw Error("Not connectable.");
                if (mode === "memory") {
                    userData.memoryHighScore = score;
                } else {
                    userData.triviaHighScore = score;
                }
            }).catch((e: Error) => {
                console.log(e.message);
                return e.message;
            });
        }
    }

    return ({ compareScore });
}

