import { ChampionInfo } from "components/ChampionData/ChampionData";
import React, {createContext, ReactElement, useContext} from "react";

interface ChampionDataContextProps {
    championDataMap: Map<String, ChampionInfo>,
    loading: boolean,
    error: boolean,
    championNames: string[]
}

export const ChampionDataContext = createContext<ChampionDataContextProps | null>(null);
export const ChampionDataContextProvider: React.FC<{
    value: ChampionDataContextProps;
    children: ReactElement
}> = ({ value, children}) => {
    return (
        <ChampionDataContext.Provider value={value}>
            {children}
        </ChampionDataContext.Provider>
    );
};

export function useChampionDataContext(): ChampionDataContextProps {
    return useContext(ChampionDataContext) as ChampionDataContextProps;
}