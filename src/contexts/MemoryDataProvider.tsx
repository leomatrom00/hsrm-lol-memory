import {createContext, ReactNode, useCallback, useContext, useState} from "react";
import {wait} from "@testing-library/user-event/dist/utils";
import {DIFFICULTIES} from "../components/MemoryMode/MemoryBoard/Difficulty";
import {useSaveScore} from "../hooks/useSaveScore";

interface CardInfo {
    name: string,
    setCompleted: (val: boolean) => void,
    setShow: (val: boolean) => void,
}

export const MemoryDataContext = createContext<any | undefined>(undefined);

export function MemoryDataProvider({ children }: { children: ReactNode}) {
    const { compareScore } = useSaveScore();

    // Game states
    const [isStarted, setIsStarted] = useState<boolean>(false);
    const [isFinished, setIsFinished] = useState<boolean>(false);

    //Game infos
    const [startTime, setStartTime] = useState<number>(0);
    const [score, setScore] = useState<number>(0);

    // Card behaviour
    let selectedCards: Array<CardInfo> = [];
    const [fieldBlocked, setFieldBlocked] = useState<boolean>(false);
    const [cardList, setCardList] = useState<string[][]>([]);
    const [foundPairs, setFoundPairs] = useState<number>(0);

    const createGame = useCallback((cardCount: number, champCards: any[]) => {
        setIsStarted(true);
        setCardList(prepareMemoryCards(cardCount, champCards));
        setFoundPairs(0);
        setStartTime(Date.now());
        setScore(0);
    }, []);

    const selectCard = ((name: string, setShow: (val: boolean) => void, setCompleted: (val: boolean) => void) => {
        const card: CardInfo = {
            name: name,
            setCompleted: setCompleted,
            setShow: setShow,
        };

        selectedCards.push(card);

        if(selectedCards.length === 2) {
            setFieldBlocked(true)

            if(compareCards(selectedCards)) {
                raiseScore(1000);
                selectedCards.forEach((elem) => {
                    elem.setCompleted(true);
                })
                setFoundPairs((foundPairs+1));
            }

            wait(300).then(() => {
                selectedCards.forEach((elem) => {
                    elem.setShow(false);
                })
                selectedCards = [];
                setFieldBlocked(false);
            })
        }
    })

    const raiseScore = (val: number) => {
        setScore((score+val));
    }

    const getScore = useCallback(() => {
        return score.toString().padStart(3, "0");
    }, [score]);

    const getCurrentTime = useCallback(() => {
        const time = new Date(Date.now() - startTime)
        return (time.getMinutes().toString().padStart(2, "0") + ":" + time.getSeconds().toString().padStart(2, "0"));
    }, [startTime]);

    const finishGame = useCallback((setFeedback: (val: string) => void) => {
        setIsFinished(true);
        setIsStarted(false);
        const time = getCurrentTime();
        const timeVals = time.split(":");
        const finalScore = Math.round(parseInt(getScore()) / (parseInt(timeVals[0])*60 + parseInt(timeVals[1])));
        const returnVal = compareScore(finalScore, "memory");
        returnVal.then((result) => {
            if(!result) {
                setFeedback("Gewonnen! Dein finaler Score beträgt " + finalScore);
            } else {
                setFeedback("Gewonnen! Jedoch konnte dein Score von " + finalScore + " nicht gespeichert werden.");
            }
        })
    }, [getCurrentTime, getScore, compareScore]);


    return (
        <MemoryDataContext.Provider value={{ fieldBlocked, cardList, foundPairs, isStarted, isFinished, createGame, selectCard, getScore, getCurrentTime, finishGame }}>
            {children}
        </MemoryDataContext.Provider>
    )
}

function shuffle(elements: any[]) {
    let j, x, i;
    for (i = elements.length - 1; i > 0; i--) {
        j = Math.floor(Math.random() * (i + 1));
        x = elements[i];
        elements[i] = elements[j];
        elements[j] = x;
    }
    return elements;
}

function prepareMemoryCards(difficulty: DIFFICULTIES | undefined, cardData: string[][]) {
    cardData = shuffle(cardData).slice(0, difficulty !== undefined ? difficulty : DIFFICULTIES.EASY);
    return shuffle(cardData.flatMap(url => [url,url]));
}

function compareCards (selection: CardInfo[]):boolean {
    return selection[0].name === selection[1].name;
}

export const useGameData = () => useContext(MemoryDataContext);
