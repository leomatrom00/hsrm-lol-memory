import { createContext, ReactNode, useCallback, useContext, useState } from "react";
import { wait } from "@testing-library/user-event/dist/utils";
import { useSaveScore } from "../hooks/useSaveScore";
import { QUESTION_TYPE } from "components/TriviaMode/Question/QuestionType";
import { ANSWER_TYPE } from "components/TriviaMode/AnswerField/AnswerType";
import { ChampionInfo } from "components/ChampionData/ChampionData";

export interface QuestionData {
    answerType: ANSWER_TYPE,
    header: string
    correctAnswer: string,
    answerOptions: string[]
}

interface TriviaProps {
    currentQuestion: QuestionData,
    isStarted: boolean,
    isFinished: boolean,
    createGame: (questionCount: number, championDataMap: Map<String, ChampionInfo>, championNames: String[]) => void,
    selectAnswer: (answer: string) => void,
    getScore: () => string,
    blocked: boolean,
}

export const TriviaDataContext = createContext<any | undefined>(undefined);

export function TriviaDataProvider({ children }: { children: ReactNode }) {
    const { compareScore } = useSaveScore();

    // Game states
    const [isStarted, setIsStarted] = useState<boolean>(false);
    const [isFinished, setIsFinished] = useState<boolean>(false);

    //Game infos
    const [score, setScore] = useState<number>(0);
    const [currentQuestion, setCurrentQuestion] = useState<QuestionData | null>(null);
    const [currentQuestionIndex, setCurrentQuestionIndex] = useState<number>(0);
    const [allQuestions, setAllQuestions] = useState<QuestionData[]>([]);
    const [blocked, setBlocked] = useState<boolean>(false);

    const createGame = useCallback((questionCount: number, championDataMap: Map<String, ChampionInfo>, championNames: String[]) => {
        setScore(0);
        setIsStarted(true);
        setIsFinished(false);

        const questions = prepareQuestions(questionCount, championNames, championDataMap);
        if (questions) {
            setAllQuestions(questions);
            setCurrentQuestion(questions[0]);
            setCurrentQuestionIndex(1);
        }
    }, []);

    const getScore = useCallback(() => {
        return score.toString();
    }, [score]);

    const finishGame = useCallback(() => {
        setIsFinished(true);
        setIsStarted(false);
        return compareScore(parseInt(getScore()), "trivia");
    }, [getScore, compareScore]);

    const nextQuestion = useCallback(() => {
        if (currentQuestionIndex < allQuestions.length) {
            setCurrentQuestion(allQuestions[currentQuestionIndex]);
            setCurrentQuestionIndex(currentQuestionIndex + 1);
            return;
        }
        finishGame();
    }, [finishGame, currentQuestionIndex, allQuestions]);

    const raiseScore = useCallback((val: number) => {
        setScore((score + val));
    }, [score]);

    const selectAnswer = useCallback((name: string) => {
        setBlocked(true);
        if (currentQuestion?.correctAnswer === name) {
            raiseScore(1);
        }
        console.log("Wait");
        wait(2000).then(() => {
            console.log("Get next question");
            nextQuestion();
            setBlocked(false);
        })
    }, [nextQuestion, currentQuestion, raiseScore]);


    return (
        <TriviaDataContext.Provider value={{ currentQuestion, isStarted, isFinished, createGame, selectAnswer, getScore, finishGame, blocked }}>
            {children}
        </TriviaDataContext.Provider>
    )
}

function shuffle(elements: any[]) {
    let j, x, i;
    for (i = elements.length - 1; i > 0; i--) {
        j = Math.floor(Math.random() * (i + 1));
        x = elements[i];
        elements[i] = elements[j];
        elements[j] = x;
    }
    return elements;
}

function sort(elements: any[] | undefined) {
    return elements ? elements.sort((n1, n2) => {
        if (n1 > n2) {
            return 1;
        }

        if (n1 < n2) {
            return -1;
        }

        return 0;
    }) : elements;
}

function prepareQuestions(questionCount: number, championNames: String[], championDataMap: Map<String, ChampionInfo>): QuestionData[] {
    const createdQuestions: QuestionData[] = [];
    const numOfChampions = championNames.length;

    let chosenQuestionType;
    let chosenChampionRight: string;
    let header: string;
    let answerPossibilities: string[];
    let createdQuestion: QuestionData;
    let searchedFor: string;

    while (createdQuestions.length !== questionCount) {
        try {
            // 1. PICK QUESTION TYPE and ANSWER TYPE
            chosenQuestionType = getRandomQuestionType(QUESTION_TYPE);

            // 2. PICK CHAMPION FOR RIGHT ANSWER, PREPARE HEADER
            do {
                chosenChampionRight = championNames[Math.floor(Math.random() * (numOfChampions))].toString();
            } while(createdQuestions.map((question) => question.correctAnswer).find((name) => name === chosenChampionRight));
            header = parseQuestionHeader(championDataMap.get(chosenChampionRight), chosenQuestionType);

            // 3. LOOK FOR NON EQUAL CHAMPIONS FOR WRONG ANSWERS
            searchedFor = header.split(":")[0].trim();
            answerPossibilities = lookForAnswers(championDataMap, searchedFor, chosenQuestionType, chosenChampionRight);

            // 4. SHUFFLE ANSWERS
            answerPossibilities = shuffle(answerPossibilities);

            // 5. create question
            createdQuestion = {
                answerType: ANSWER_TYPE.CHAMPION_NAME,
                header: header,
                correctAnswer: chosenChampionRight,
                answerOptions: answerPossibilities
            };
            createdQuestions.push(createdQuestion);
        } catch (error) {
            console.log(error);
        }
    }
    return createdQuestions;
}

export function parseQuestionHeader(championData: ChampionInfo | undefined, questionType: QUESTION_TYPE): string {

    switch (questionType) {
        case QUESTION_TYPE.CHAMPION_ROLES:
            return `Welcher Champion besitzt die folgenden Rollen: ${sort(championData?.tags)?.map((tag) => tag).join(", ")}`;
        case QUESTION_TYPE.CHAMPION_TITLE:
            return `Welcher Champion steckt hinter diesem Titel: ${championData?.title}`;
        default:
            return `Frage nicht definiert. Fehler!`;
    }
}

function lookForAnswers(championDataMap: Map<String, ChampionInfo | undefined>, searchedFor: string, chosenQuestionType: QUESTION_TYPE, answer: string): string[] {
    const answers: string[] = [answer];
    let championsLeft = Array.from(championDataMap.keys()).filter((name) => name !== answer);
    while (answers.length !== 4) {
        if (!championsLeft.length) {
            throw new Error(`Not enough champions found for answer ${answer}`);
        }
        let currentChosen = championDataMap.get(championsLeft[Math.floor(Math.random() * championsLeft.length)]);
        switch (chosenQuestionType) {
            case QUESTION_TYPE.CHAMPION_TITLE:
                if (currentChosen !== undefined && currentChosen.title !== searchedFor) {
                    answers.push(currentChosen.name);
                }
                break;
            case QUESTION_TYPE.CHAMPION_ROLES:
                if (currentChosen !== undefined && sort(currentChosen.tags)?.map((tag) => tag).join(", ") !== searchedFor) {
                    answers.push(currentChosen.name);
                }
                break;
            default: break;
        }
        championsLeft = championsLeft.filter((champName) => champName !== currentChosen?.name);
    }
    return answers;
}

function getRandomQuestionType<T>(anEnum: T): T[keyof T] {
    const enumValues = Object.keys(anEnum)
        .map(n => Number.parseInt(n))
        .filter(n => !Number.isNaN(n)) as unknown as T[keyof T][]
    const randomIndex = Math.floor(Math.random() * enumValues.length)
    const randomEnumValue = enumValues[randomIndex]
    return randomEnumValue;
}

export function useTriviaGameData(): TriviaProps {
    return useContext(TriviaDataContext) as TriviaProps;
}