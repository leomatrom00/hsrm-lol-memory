import {createContext, ReactNode, useContext, useState} from "react";
import placeholder from "../resources/images/placeholder_profile.png";

interface UserData {
    blocked: boolean,
    confirmed: boolean,
    createdAt: string,
    email: string,
    id: number,
    memoryHighScore: number,
    provider: string,
    triviaHighScore: number,
    updatedAt: string,
    username: string,
    profileImage: string,
}

export const LoginContext = createContext<any | undefined>(undefined);

export function LoginProvider({ children }: { children: ReactNode}) {
    const [isLoggedIn, setIsLoggedIn] = useState(false);
    const [userData, setUserData] = useState<UserData|undefined>();

    const login = (data: UserData) => {
        setIsLoggedIn(true);
        setUserData(data);
        if(data.profileImage == null) {
            data.profileImage = placeholder;
        }
    };

    const logout = () => {
        setIsLoggedIn(false);
        setUserData(undefined);
        sessionStorage.removeItem("apiToken");
    };

    const setProfileImage = (image: string) => {
        console.log(userData);
        if(userData) {
            userData.profileImage = image;
        }
    }

    return (
        <LoginContext.Provider value={{ isLoggedIn, login, logout, userData, setProfileImage }}>
            {children}
        </LoginContext.Provider>
    )
}

export const useLogin = () => useContext(LoginContext);
