import React from "react";
import ChampionDataList from "../ChampionDataList/ChampionDataList";
import IntroText from "components/Introtext/Introtext";
import LoginButton from "components/LoginRegistration/LoginButton/LoginButton";
import {useLogin} from "contexts/LoginProvider";
import "../style/Home.scss"

export const Home: React.FunctionComponent = () => {
    const { isLoggedIn } = useLogin()

    const header = "Runeterra Boardgames";
    const text = ["Messe dich mit anderen Spielern im Memory oder zeige dein Wissen über die Welt von Runeterra im Trivia Quiz.",
        (!isLoggedIn ? `Du hast noch keinen Account? Starte jetzt deine Laufbahn als Boardgamer.`: "" )];

    // additionalElem={isLoggedIn ? undefined : <LoginButton transState={false} />}
    return <div className="home">
        <IntroText introHead={header} introText={text} loginRelevance={false} />
        <ChampionDataList/>
    </div>;
}

export default Home;