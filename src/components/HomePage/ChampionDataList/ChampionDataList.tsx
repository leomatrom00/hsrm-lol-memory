import ChampionData from "components/ChampionData/ChampionData";
import { useChampionDataContext } from "contexts/ChampionDataContext";
import { Card, Col, Row} from "react-bootstrap";

export const ChampionDataList = () => {
    const { championDataMap, loading, error, championNames } = useChampionDataContext();

    if (loading) {
        return (<div>Sammle Daten...</div>);
    } else if (error) {
        return (<div>Ein unerwarteter Fehler ist aufgetreten...</div>);
    } else {
        return (<div className="championList pt-3">
            <h2 className="flex-start mb-4">Liste aller Champions</h2>
            <Row xs={1} md={2} lg={4} className="championList--list g-3">
                {championNames.map((championName, key) => {
                    return (
                        <Col key={key}>
                            <Card className="champCard bg-transparent text-center">
                                <ChampionData information={championDataMap.get(championName)} />
                            </Card>
                        </Col>);
                })}
            </Row>
        </div>);
    };
}

export default ChampionDataList;