import { ENGLISH_DATA_URL, fetchChampionData, useChampionData } from "./useChampionData";

import { mockData } from "resources/testData/championData";

describe("fetch champion data", () => {
  const fetcher = jest.fn();  
  fetcher.mockImplementation(() =>
      Promise.resolve({
        ok: true,
        json: () => Promise.resolve(mockData),
      })
    );

    const { championDataMap, championNames, loading, error } = useChampionData();   

    it("should fetch on the right URL", async () => {
        expect(fetcher).toBeCalledWith(ENGLISH_DATA_URL);
        expect(fetcher).toBeCalledWith(fetchChampionData);
    });

    it("should check that every data is correct", async () => {
        expect(championDataMap).toBeTruthy();
        expect(championDataMap?.size).toEqual(championNames.length);
        expect(loading).toBeFalsy();
        expect(error).toBeFalsy();
    });
});
