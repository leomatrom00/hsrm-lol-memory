import { ChampionInfo } from 'components/ChampionData/ChampionData';
import { useState, useEffect } from 'react';
import { mockData } from 'resources/testData/championData';

export const ENGLISH_DATA_URL = "https://ddragon.leagueoflegends.com/cdn/12.13.1/data/en_US/champion.json";

export const useChampionData = () => {
    const [championDataMap, setChampionDataMap] = useState<Map<String, ChampionInfo>>(new Map());
    const [error, setError] = useState(false);

    useEffect(() => {
        const championCallback = async () => { return await fetchChampionData(); }
        championCallback().then((responseData) => {
            if (responseData) {
                setError(false);
                setChampionDataMap(responseData);
            } else {
                setError(true);
            }
        });

    }, []);

    return {
        championDataMap: championDataMap,
        error: error,
        loading: championDataMap === null,
        championNames: championDataMap !== null ? Array.from(championDataMap.keys()) : []
    }
};

export async function fetchChampionData(): Promise<Map<String, ChampionInfo> | null> {
    try {
        const fetchedData: any = await fetch(ENGLISH_DATA_URL)
        .then((response) => response.json().then((jsonData) => jsonData.data)).catch(() => mockData.data);
        const names: string[] = Object.keys(fetchedData);
        const championDataAsMap = new Map<String, ChampionInfo>();
        names.map((champName) => {
            championDataAsMap.set(champName, fetchedData[champName]);
            return champName;
        });
        return championDataAsMap;
    } catch (error) {
        console.log(error);
        return null;
    }
}