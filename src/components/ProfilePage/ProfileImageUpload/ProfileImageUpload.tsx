import React, {useState} from "react";
import ReactImageUploading, {ImageListType, ImageType} from "react-images-uploading";
import {useLogin} from "../../../contexts/LoginProvider";

interface ProfileImageUploadProps {
    setIsPreview: (val :boolean) => void
}

export const ProfileImageUpload: React.FunctionComponent<ProfileImageUploadProps> = ({setIsPreview}) => {
    const [imageFile, setImage] = useState<ImageType>();
    const { setProfileImage } = useLogin();

    const onChange = (image: ImageListType) => {
        // data for submit
        console.log(image);
        setImage(image[0]);
        setIsPreview(true);
    };

    return <div className="profile--imageUpload">
        <ReactImageUploading
            multiple
        value={imageFile ? [imageFile] : []}
        onChange={onChange}
        maxNumber={1}
    >
        {({imageList,onImageUpload,onImageRemove,isDragging,dragProps}) => (
            // write your building UI
            <div className="upload__image-wrapper g-5">
                <button className="upload-button btn btn-outline-light"
                    style={isDragging ? { color: "red" } : undefined}
                    onClick={onImageUpload}
                    {...dragProps}
                >
                    Profilbild ändern
                </button>
                &nbsp;
                {imageList.map((image, index) => (
                    <div key={index} className="image-item">
                        <img src={image.dataURL} alt="Profilbild Vorschau" className="image-item__preview" />
                        <div className="image-item__btn-wrapper">
                            <button className="btn-wrapper-button btn btn-outline-light" onClick={() => {
                                onImageRemove(index);
                                setIsPreview(false);
                                setProfileImage(image.dataURL);
                            }}>Speichern</button>
                            <button className="btn-wrapper-button btn btn-outline-light" onClick={() => {
                                onImageRemove(index);
                                setIsPreview(false);
                            }}>Verwerfen</button>
                        </div>
                    </div>
                ))}
            </div>
        )}
        </ReactImageUploading></div>;
}

export default ProfileImageUpload;