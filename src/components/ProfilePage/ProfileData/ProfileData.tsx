import React, {useState} from "react";
import {useLogin} from "contexts/LoginProvider";
import ProfileImageUpload from "../ProfileImageUpload/ProfileImageUpload";
import {Col, Row} from "react-bootstrap";

export const ProfileData: React.FunctionComponent = () => {
    const { userData } = useLogin();
    const [isPreview, setIsPreview] = useState<boolean>(false);

    return <Row className="profile--content" >
            <Col md={12} lg={6} className="profile--content__image d-flex flex-column">
                { !isPreview ? <img src={userData.profileImage} alt="Profilbild" className="profile-image"/> : ""}
                <ProfileImageUpload setIsPreview={setIsPreview}/>
            </Col>
            <Col md={12} lg={6} className="profile--content__data d-flex flex-column">
                <div className="profile-data"><span className="profile-data--label">Nutzername:</span><span className="profile-data--data">{ userData.username }</span></div>
                <div className="profile-data"><span className="profile-data--label">E-Mail Adresse:</span><span className="profile-data--data">{ userData.email }</span></div>
                <div className="profile-data"><span className="profile-data--label">Account Status:</span><span className="profile-data--data">{ userData.confirmed ? "Bestätigt" : "Noch nicht bestätigt"}</span></div>
                <div className="profile-data"><span className="profile-data--label">Erstellt am:</span><span className="profile-data--data">{ (userData.createdAt).toString().split("T")[0] }</span></div>
                <div className="profile-highscoreDisplay">
                    <div className="profile-data h4"><span className="profile-data--label">Memory Highscore:</span><span className="profile-data--data">{ userData.memoryHighScore }</span></div>
                    <div className="profile-data h4"><span className="profile-data--label">Triviaquiz Highscore:</span><span className="profile-data--data">{ userData.triviaHighScore }</span></div>
                </div>
            </Col>
        </Row>;
}

export default ProfileData;