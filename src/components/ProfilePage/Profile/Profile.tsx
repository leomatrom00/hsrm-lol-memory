import React from "react";
import {useLogin} from "contexts/LoginProvider";
import ProfileData from "../ProfileData/ProfileData";
import IntroText from "components/Introtext/Introtext";
import "../style/Profile.scss"

export const Profile: React.FunctionComponent = () => {
    const { userData, isLoggedIn } = useLogin();

    if(!isLoggedIn || !userData) {
        return <IntroText introHead="" introText={[]} />
    }

    return <div className="profile">
        <div className="profile--header">
            <div className="profile--header__text" >Das ist dein Profil, {userData.username}</div>
        </div>
        <ProfileData />
    </div>;
}

export default Profile;