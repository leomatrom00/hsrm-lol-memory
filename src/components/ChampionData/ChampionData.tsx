import React, {useState} from "react";
import { Card } from "react-bootstrap";
import { IMAGE_URL_PREFIX } from "hooks/Constants"

class Info {
    attack: number;
    defense: number;
    magic: number;
    difficulty: number;

    constructor(attack: number, defense: number, magic: number, difficulty: number) {
        this.attack = attack;
        this.defense = defense;
        this.magic = magic;
        this.difficulty = difficulty;
    }
}

class ChampionImage {
    full: string;
    sprite: string;
    group: string;
    x: number;
    y: number;
    w: number;
    h: number;

    constructor(full: string, sprite: string, group: string, x: number, y: number, w: number, h: number) {
        this.full = full;
        this.sprite = sprite;
        this.group = group;
        this.x = x;
        this.y = y;
        this.w = w;
        this.h = h;
    }
}

class Stats {
    hp: number;
    hpperlevel: number;
    mp: number;
    mpperlevel: number;
    movespeed: number;
    armor: number;
    armorperlevel: number;
    spellblock: number;
    spellblockperlevel: number;
    attackrange: number;
    hpregen: number;
    hpregenperlevel: number;
    mpregen: number;
    mpregenperlevel: number;
    crit: number;
    critperlevel: number;
    attackdamage: number;
    attackdamageperlevel: number;
    attackspeedperlevel: number;
    attackspeed: number;

    constructor(hp: number, hpperlevel: number, mp: number, mpperlevel: number, movespeed: number, armor: number, armorperlevel: number, spellblock: number, spellblockperlevel: number, attackrange: number, hpregen: number, hpregenperlevel: number, mpregen: number, mpregenperlevel: number, crit: number, critperlevel: number, attackdamage: number, attackdamageperlevel: number, attackspeedperlevel: number, attackspeed: number) {
        this.hp = hp;
        this.hpperlevel = hpperlevel;
        this.mp = mp;
        this.mpperlevel = mpperlevel;
        this.movespeed = movespeed;
        this.armor = armor;
        this.armorperlevel = armorperlevel;
        this.spellblock = spellblock;
        this.spellblockperlevel = spellblockperlevel;
        this.attackrange = attackrange;
        this.hpregen = hpregen;
        this.hpregenperlevel = hpregenperlevel;
        this.mpregen = mpregen;
        this.mpregenperlevel = mpregenperlevel;
        this.crit = crit;
        this.critperlevel = critperlevel;
        this.attackdamage = attackdamage;
        this.attackdamageperlevel = attackdamageperlevel;
        this.attackspeedperlevel = attackspeedperlevel;
        this.attackspeed = attackspeed;
    }
}

export class ChampionInfo {
    version: string;
    id: string;
    key: string;
    name: string;
    title: string;
    blurb: string;
    info: Info;
    imageData: ChampionImage;
    tags: Array<string>;
    partype: string;
    stats: Stats;

    constructor(version: string, id: string, key: string, name: string, title: string, blurb: string, info: Info, imageData: ChampionImage, tags: Array<string>, partype: string, stats: Stats) {
        this.version = version;
        this.id = id;
        this.key = key
        this.name = name;
        this.title = title;
        this.blurb = blurb;
        this.info = info;
        this.imageData = imageData;
        this.tags = tags;
        this.partype = partype;
        this.stats = stats;
    }
}

export interface ChampionNameInfo {
    information: ChampionInfo | undefined
}

export const ChampionData: React.FunctionComponent<ChampionNameInfo> = ({information}) => {
    const [ showImage, setShowImage ] = useState<boolean>(false);

    function handleClick() {
        setShowImage(!showImage);
    }

    return <Card.Body as="a" className="listChamp" onClick={() => handleClick()}>
        <span>{information?.name}</span>
        { showImage ?
            (<div>
                <hr />
                <img src={IMAGE_URL_PREFIX + information?.id + ".png"} alt={information?.name + " Bild"} />
            </div>) : ""
        }
    </Card.Body>
};

export default ChampionData;