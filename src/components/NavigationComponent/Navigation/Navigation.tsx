import Logo from "../NavgiationPartials/Logo";
import branding from "resources/images/logo-1.png";
import {Container, Nav, Navbar} from "react-bootstrap";
import NavigationItem from "../NavgiationPartials/NavigationItem";
import LoginButton from "components/LoginRegistration/LoginButton/LoginButton";
import "../style/Navigation.scss"

export const Navigation = () => {
    const navItems = ["memory", "trivia"];

    function switchActive(activElem: HTMLElement) {
        const deactivElem = document.querySelector(".nav-link.active");
        deactivElem?.classList.remove("active");

        const attributes = activElem.getAttribute("class");
        activElem.setAttribute("class", attributes + " active");
    }

    return (
        <Navbar bg="dark" variant="dark" expand="xl" sticky="top">
            <Container >
                <Logo filePath={branding} switchActive={switchActive}/>
                <Navbar.Toggle aria-controls="responsive-navbar-nav"/>
                <Navbar.Collapse id="responsive-navbar-nav">
                    <Nav className="me-auto">
                        {
                            navItems.map((navItem, index) => {
                               return <NavigationItem key={index} name={navItem} switchActive={switchActive}/>
                            })
                        }
                    </Nav>
                    <LoginButton transState={false} switchActive={switchActive} />
                </Navbar.Collapse>
            </Container>
        </Navbar>
        );

}

export default Navigation;
