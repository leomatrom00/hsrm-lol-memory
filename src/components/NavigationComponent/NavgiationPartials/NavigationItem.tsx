import React from 'react';
import {Nav} from "react-bootstrap";
import { Link } from 'react-router-dom';

interface Item {
    name: string,
    switchActive: (val: HTMLElement) => void,
}

export const NavigationItem: React.FC<Item> = ({name, switchActive}) => {

    return (
        <Nav.Link as={Link} to={name} onClick={(e) => switchActive(e.currentTarget)} aria-label={`navigationItem${name}`}>{name}</Nav.Link>
    )
}

export default NavigationItem;
