import React from 'react';
import { Navbar } from 'react-bootstrap';
import {Link} from "react-router-dom";

interface Image {
    filePath: string,
    switchActive: (val: HTMLElement) => void
}

export const Logo: React.FC<Image> = ({filePath, switchActive}) => {

    return (
        <Navbar.Brand className='brand-wrapper' to="home" as={Link} onClick={(event) => switchActive(event.currentTarget)}>
            <img src={filePath} className="img-fluid d-inline-block align-top" alt=""/>
        </Navbar.Brand>
    )
}

export default Logo;
