import React, { useState } from "react";
import { Card } from "react-bootstrap";
import { useGameData } from "../../../contexts/MemoryDataProvider";

interface MemoryCardProps {
    imageURL: string,
    name: string,
    blocked: boolean,
}

export const MemoryCard: React.FunctionComponent<MemoryCardProps> = ({ imageURL, name = "Champion", blocked = false }: MemoryCardProps) => {
    const [showMemoryCard, setShowMemoryCard] = useState<boolean>(false);
    const [completed, setCompleted] = useState<boolean>(false);
    const { selectCard } = useGameData();

    function handleClick() {
        setShowMemoryCard(!showMemoryCard);
    }

    return (
        <Card as="a" className="champCard bg-transparent" aria-label="memoryCardClickable"
            style={{ cursor: "pointer" }}
            onClick={() => { !showMemoryCard && !blocked && !completed && handleClick() }}>
            <Card.Body className="memory-card" >
                {(showMemoryCard || completed) ? <img onLoad={() => selectCard(name, setShowMemoryCard, setCompleted)} src={imageURL} alt={name} className={name + " memory-card__front"} aria-label="memoryCardImage"/> : ""}
                <div className="memory-card__back" aria-label="memoryCardback"/>
            </Card.Body>
        </Card>);
}

export default MemoryCard;