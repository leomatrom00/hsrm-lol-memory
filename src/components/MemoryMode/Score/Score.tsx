import React from "react";

interface ScoreProps {
    score: number,
    currentTime: string,
}

export const Score: React.FunctionComponent<ScoreProps> = ({score, currentTime}) => {

    return (<div className="memory--gamescore">
        <div>{score}</div>
        <div>{currentTime}</div>
    </div>);

}



export default Score;