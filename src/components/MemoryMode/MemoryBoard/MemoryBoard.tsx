import React, {useEffect, useState} from "react";
import MemoryCard from "../MemoryCard/MemoryCard";
import {Col, Row} from "react-bootstrap";
import {useGameData} from "../../../contexts/MemoryDataProvider";
import Score from "../Score/Score";
import {DIFFICULTIES} from "./Difficulty";

interface MemoryBoardProps {
    memoryCardData: string[][],
    names: string[],
    difficulty: DIFFICULTIES | undefined,
    setFeedback: (val: string) => void,
    setGameProcessing: (val: boolean) => void,
}

export const MemoryBoard: React.FC<MemoryBoardProps> = ({memoryCardData, difficulty, setFeedback, setGameProcessing}) => {
    const {fieldBlocked, cardList, foundPairs, isStarted, isFinished, getScore, getCurrentTime, createGame, finishGame} = useGameData();
    const [time, setTime] = useState<string>("00:00");

    useEffect(() => {
        const timer = setTimeout(() => {
            if(isFinished) {
                return () => clearTimeout(timer);
            }
            setTime(getCurrentTime());
        }, 1000);

        return () => clearTimeout(timer);
    });

    useEffect(() => {
        createGame(difficulty, memoryCardData);
    }, [memoryCardData, difficulty, createGame]);

    if(isStarted && foundPairs >= (difficulty ? difficulty : DIFFICULTIES.EASY)) {
        finishGame(setFeedback);
        setGameProcessing(false);
    }

    return (<div className="memory--gameboard" aria-label="memoryBoard">
        <Score score={getScore()} currentTime={time} />
        <Row xs={1} md={2} lg={4} className="memory--items">
            {cardList.map((cardData: string[], key: number) => {
                return (<Col className="g-3" key={key}>
                    <MemoryCard imageURL={cardData[0]} name={cardData[1]} blocked={fieldBlocked} />
                </Col>);
            })}
        </Row>
    </div>);

}

export default MemoryBoard;