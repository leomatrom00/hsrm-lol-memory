export const enum DIFFICULTIES {
    EASY = 6,
    MEDIUM = 12,
    HARD = 24
}