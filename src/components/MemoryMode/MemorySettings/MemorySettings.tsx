import React, { useState } from "react";
import { DIFFICULTIES } from "../MemoryBoard/Difficulty";
import { Button, ButtonGroup, Dropdown } from "react-bootstrap";
import DropdownItem from "react-bootstrap/DropdownItem";
import DropdownToggle from "react-bootstrap/DropdownToggle";
import DropdownMenu from "react-bootstrap/DropdownMenu";

interface MemorySettingsProps {
    setFeedback: (param: string) => void,
    setDifficulty: (param: DIFFICULTIES) => void,
    setGameStart: (param: boolean) => void,
}

export const MemorySettings: React.FunctionComponent<MemorySettingsProps> = ({ setFeedback, setDifficulty, setGameStart }) => {
    const [selected, setSelected] = useState<string>("Schwierigkeit");

    const handleSelect = ((selection: any) => {

        setSelected(selection);

        switch (selection) {
            case "Einfach":
                setDifficulty(DIFFICULTIES.EASY);
                break;
            case "Mittel":
                setDifficulty(DIFFICULTIES.MEDIUM);
                break;
            case "Schwer":
                setDifficulty(DIFFICULTIES.HARD);
                break;
            default:
                break;
        }
    })

    return <div className="memory--settings">
        <Button variant="outline-light memory-gameStart" onClick={() => {
            setFeedback("");
            setGameStart(true)}
        } aria-label="memoryStartButton">Spiel starten</Button>
        <Dropdown as={ButtonGroup} onSelect={handleSelect} aria-label="memoryDifficultyDropdown">
            <DropdownToggle variant="outline-light" className="memory-dropdown text-end" aria-label="memoryDifficultyDropdownValue">{selected}</DropdownToggle>
            <DropdownMenu variant="dark" className="memory-dropdown-menu text-end">
                <DropdownItem eventKey="Einfach" aria-label="memorySettingsEasy">Einfach</DropdownItem>
                <Dropdown.Divider />
                <DropdownItem eventKey="Mittel" aria-label="memorySettingsMedium">Mittel</DropdownItem>
                <Dropdown.Divider />
                <DropdownItem eventKey="Schwer" aria-label="memorySettingsHard">Schwer</DropdownItem>
            </DropdownMenu>
        </Dropdown>
    </div>;
}

export default MemorySettings;