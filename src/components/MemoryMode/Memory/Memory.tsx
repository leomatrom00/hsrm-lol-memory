import React, {useState} from "react";
import {useMemorySessionState} from "hooks/useMemorySessionState";
import {useChampionDataContext} from "contexts/ChampionDataContext";
import {MemoryDataProvider} from "contexts/MemoryDataProvider";
import {MemoryBoard} from "../MemoryBoard/MemoryBoard";
import IntroText from "../../Introtext/Introtext";
import MemorySettings from "../MemorySettings/MemorySettings";
import {useLogin} from "contexts/LoginProvider";
import "../style/Memory.scss"

export const Memory: React.FunctionComponent = () => {
    const { championDataMap, championNames } = useChampionDataContext();
    const { memoryCardData, difficulty, setDifficulty } = useMemorySessionState({championDataMap, championNames});
    const [gameProcessing, setGameProcessing] = useState<boolean>(false);
    const [feedback, setFeedback] = useState<string>("");
    const { isLoggedIn } = useLogin();

    // Texts
    const header = "Wilkommen zum Memory";
    const text = ["Sei schneller als Yasuo sein Schwert ziehen kann und beweise jetzt, dass du ein Meister im Schnell-Ziehen und richtigen Kombinieren der Memory Karten bist"]

    if (!isLoggedIn) {
        return <IntroText introHead={header} introText={text} />
    }

    return <div className="memory">
        <IntroText introHead={header} introText={text} />
        <IntroText introHead={feedback} introText={feedback ? ["Möchtest du nochmal spielen?"] : []} bgStyle="success" headerStyle="h2" />
        {gameProcessing ?
            <MemoryDataProvider>
                <MemoryBoard memoryCardData={memoryCardData} names={championNames} difficulty={difficulty} setFeedback={setFeedback} setGameProcessing={setGameProcessing} />
            </MemoryDataProvider> :
            <MemorySettings setFeedback={setFeedback} setDifficulty={setDifficulty} setGameStart={setGameProcessing}/>
        }
    </div>;
}
    
export default Memory;