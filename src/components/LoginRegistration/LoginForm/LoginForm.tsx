import {Button, Form, FormControl, FormLabel} from "react-bootstrap";
import React, {useState} from "react";
import FormFeedback from "../FormFeedback/FormFeedback";
import {useLoginData} from "hooks/useLoginData";

interface LoginFormProps {
    onClose: () => void
}

export const LoginForm: React.FC<LoginFormProps> = ({onClose}) => {
    const [username, setUsername] = useState<string>("");
    const [password, setPassword] = useState<string>("");
    const { authorize, feedback } = useLoginData();

    const handleSubmit = (event: any) => {
        event.preventDefault();
        authorize(username, password).then((result) => {
            if(result) onClose();
        });
    };

    return (
        <Form className="login-form" onSubmit={handleSubmit} aria-label="loginForm">
            {feedback && <FormFeedback form="form-container--error" feedback={[feedback]} />}
            <div className="login-form--content">
                <h3 className="login-form--title">Anmelden</h3>
                <div className="form-group mt-3">
                    <FormLabel>Nutzername</FormLabel>
                    <FormControl
                        value={username}
                        onChange={(event) => setUsername(event.target.value)}
                        type="text"
                        className="form-control mt-1"
                        placeholder="Nutzername"
                        required={true}
                        aria-label="loginUsername"
                    />
                </div>
                <div className="form-group mt-3">
                    <FormLabel>Passwort</FormLabel>
                    <FormControl
                        value={password}
                        onChange={(event) => setPassword(event.target.value)}
                        type="password"
                        className="form-control mt-1"
                        placeholder="Passwort"
                        required={true}
                        aria-label="loginPassword"
                    />
                </div>
                <div className="d-grid gap-2 mt-5">
                    <Button className="form-submit" type="submit" variant="outline-light" aria-label="loginFormButton">
                        Anmelden
                    </Button>
                </div>
                <p className="forgot-password text-right mt-2">
                    <a href="#/">Passwort vergessen?</a>
                </p>
            </div>
        </Form>
    )

}

export default LoginForm;

