import {Button} from "react-bootstrap";
import React, {useState} from "react";
import LoginRegistrationTab from "../LoginRegistrationTab/LoginRegistrationTab";
import {Transition} from "react-transition-group";
import {useLogin} from "contexts/LoginProvider";
import {Link} from "react-router-dom";
import "../style/LoginRegistration.scss"

interface LoginButtonProps {
    transState: boolean,
    switchActive: (val: HTMLElement) => void,
}

export const LoginButton: React.FC<LoginButtonProps> = ({transState, switchActive}) => {
    const { userData } = useLogin();
    const [open, setOpen] = useState(false);
    const endPosition = "translateY(100px)";
    const startPosition = "translateY(0)";
    const duration = 300;
    const transitionStyles: any = {
        entering: {  opacity: 0, transform: endPosition },
        entered:  {  opacity: 1, transform: startPosition },
        exiting:  {  opacity: 0, transform: endPosition },
        exited:   {  opacity: 0, transform: endPosition },
    };

    const { logout, isLoggedIn } = useLogin()

    if(isLoggedIn) {
        return (
            <div className="loggedIn-state__wrapper">
                <Button
                    className="logout-button"
                    variant="outline-light"
                    //@ts-expect-error
                    as={Link}
                    to="home"
                    onClick={(event) => {
                        logout();
                        switchActive(event.currentTarget)
                    } }
                    aria-label="logoutButton">Abmelden</Button>
                <Button
                    variant="outline-light"
                    className="profile-icon"
                    //@ts-expect-error
                    as={Link}
                    to="profile"
                    onClick={(event) => {
                        switchActive(event.currentTarget)
                    } }
                    aria-label="profileButton"><img src={userData ? userData.profileImage : ""} alt="profile-icon"/></Button>
            </div>
        )
    }

    return (
        <div className="logReg">
            <Button
                variant="outline-light"
                onClick={(event) => {
                    setOpen(!open);
                    event.currentTarget.blur();
                } }
                aria-label="loginButton"
            >Anmelden / Registrieren</Button>
            <Transition unmountOnExit={true}
                        mountOnEnter={true}
                        in={open}
                        timeout={duration}>
                {state => (
                    <div style={transitionStyles[state]} className="logReg--container">
                        <LoginRegistrationTab onClose={() => setOpen(transState)}/>
                    </div>
                )}
            </Transition>
            {open && <div className="logReg--wrapper"/>}
        </div>
    )
}

export default LoginButton;

