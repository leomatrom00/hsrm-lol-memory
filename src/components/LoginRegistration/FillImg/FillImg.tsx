import React from 'react';
import { Navbar } from 'react-bootstrap';

interface Image {
    filePath: string,
    parent: string
}

export const FillImage: React.FC<Image> = ({filePath, parent}) => {

    return (
        <Navbar.Brand className={parent+'--image-wrapper'}>
            <img src={filePath} className="d-inline-block" alt="Poro"/>
        </Navbar.Brand>
    )
}

export default FillImage;
