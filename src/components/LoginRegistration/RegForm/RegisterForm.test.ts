import { validateRegister } from "./RegisterForm";

describe("validate register", () => {

    const validUserMail = "test@test.de";
    const invalidUserMail = "test";
    const validPassword = "T3st#8Letters"
    const invalidPassword = "test";

    let errors = validateRegister(validUserMail, validPassword, validPassword);
    it("should have 0 errors because its valid", () => {
        expect(errors.length).toBe(0);
    });

    errors = validateRegister(validUserMail, validPassword, validPassword + " ");
    it("should have 1 error because of the wrong repeatet password", () => {
        expect(errors.length).toBe(1);
    });
    it("should contain that the passwords must be the same", () => {
        expect(errors[0]).toContain("Die beiden Passwörter müssen übereinstimmen.");
    });

    errors = validateRegister(validUserMail, invalidPassword, invalidPassword);
    it("should have 1 error because of the password conditions", () => {
        expect(errors.length).toBe(1);
    });
    it("should contain the information that the password is invalid", () => {
        expect(errors[0]).toContain("Kein gültiges Passwort.");
    });

    errors = validateRegister(validUserMail, invalidPassword, invalidPassword + " ");
    it("should have two errors because password is invalid and repeated password is not equal to the original", () => {
        expect(errors.length).toBe(2);
    });
    it("should tell you that the password is invalid", () => {
        expect(errors[0]).toContain("Kein gültiges Passwort.");
    });
    it("should tell you that the passwords must be the same", () => {
        expect(errors[1]).toContain("Die beiden Passwörter müssen übereinstimmen.");
    });

    errors = validateRegister(invalidUserMail, validPassword, validPassword);
    it("should have one error because of the invalid email", () => {
        expect(errors.length).toBe(1);
    });
    it("should tell you that the pattern of the email is not valid", () => {
        expect(errors[0]).toContain("Keine gültige Email Adresse.");
    });
});
