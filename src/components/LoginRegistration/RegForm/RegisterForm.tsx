import {Button, Form, FormControl, FormLabel} from "react-bootstrap";
import React, {useState} from "react";
import FormFeedback from "../FormFeedback/FormFeedback";
import {useRegisterData} from "hooks/useRegisterData";

const mailRegex = /^(([^<>()[\],;:\s@]+(\.[^<>()[\],;:\s@]+)*)|(.+))@(([^<>()[\],;:\s@]+\.)+[^<>()[\],;:\s@]{2,})$/i;
const pwRegex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*])(?=.{8,})/;

interface RegisterFormProps {
    onClose: () => void
}

export const RegisterForm: React.FC<RegisterFormProps> = ({onClose}) => {
    const [formErrors, setFormErrors] = useState<string[]>([]);
    const [username, setUsername] = useState("");
    const [mail, setMail] = useState("");
    const [password, setPassword] = useState("");
    const [passwordRep, setPasswordRep] = useState("");
    const { register, success } = useRegisterData();

    const handleSubmit = (event: any) => {
        event.preventDefault();
        setFormErrors([]);
        const errors = validateRegister(mail, password, passwordRep);
        
        if (errors.length === 0) {
            register({username, email: mail, name: username, password}).then((result) => {
                if(result) {
                    setFormErrors([result]);
                }
            })
        } else {
            setFormErrors(errors);
        }
    };

    // TODO: feedback / Errors werden erst nach nochmaligem Submitten angezeigt
    // success funktioniert
    return (
        <Form className="register-form" onSubmit={handleSubmit} aria-label="registrationForm">
            {(formErrors) && <FormFeedback form="form-container--error" feedback={formErrors} />}
            {success && <p className="register-form--success" aria-label="registrationFormSuccess">Registrierung erfolgreich</p>}
            <div className="register-form--content">
                <h3 className="register-form--title">Registrieren</h3>
                <div className="form-group mt-3">
                    <FormLabel>Nutzername</FormLabel>
                    <FormControl
                        value={username}
                        onChange={(event) => setUsername(event.target.value)}
                        type="text"
                        className="form-control mt-1"
                        placeholder="Nutzername"
                        required={true}
                        aria-label="registrationFormUsername"
                    />
                </div>
                <div className="form-group mt-3">
                    <FormLabel>Email</FormLabel>
                    <FormControl
                        value={mail}
                        onChange={(event) => setMail(event.target.value)}
                        type="email"
                        className="form-control mt-1"
                        placeholder="Email"
                        required={true}
                        aria-label="registrationFormMail"
                    />
                </div>
                <div className="form-group mt-3">
                    <FormLabel>Passwort</FormLabel>
                    <FormControl
                        value={password}
                        onChange={(event) => setPassword(event.target.value)}
                        type="password"
                        className="form-control mt-1"
                        placeholder="Passwort"
                        required={true}
                        aria-label="registrationFormPassword"
                    />
                </div>
                <div className="form-group mt-3">
                    <FormLabel>Passwort wiederholen</FormLabel>
                    <FormControl
                        value={passwordRep}
                        onChange={(event) => setPasswordRep(event.target.value)}
                        type="password"
                        className="form-control mt-1"
                        placeholder="Passwort"
                        required={true}
                        aria-label="registrationFormPasswordRepeat"
                    />
                </div>
                <div className="d-grid gap-2 mt-5 mb-4">
                    <Button className="form-submit" type="submit" variant="outline-light" aria-label="registrationFormRegistrationButton">
                        Registrieren
                    </Button>
                </div>
            </div>
        </Form>
    )
}

export function validateRegister(mail: string, password: string, passwordRep: string): string[] {
    const errors: string[] = [];
    if(!mail.match(mailRegex)) {
        errors.push("Keine gültige Email Adresse.");
    }
    if(!password.match(pwRegex)) {
        errors.push("Kein gültiges Passwort.\n(8-20 Zeichen, mind. ein Klein- und Großbuchstabe; mind. ein Sonderzeichen; mind. eine Zahl.\n");
    } else if(password !== passwordRep) {
        errors.push("Die beiden Passwörter müssen übereinstimmen.\n");
    }
    return errors;
}

export default RegisterForm;

