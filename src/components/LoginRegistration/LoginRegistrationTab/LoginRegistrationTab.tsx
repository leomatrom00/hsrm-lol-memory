import {CloseButton, Col, Nav, Tab} from "react-bootstrap";
import React from 'react';
import LoginForm from "../LoginForm/LoginForm";
import RegisterForm from "../RegForm/RegisterForm";
import FillImage from "../FillImg/FillImg";
import poro from "resources/images/poro.jpg";
import poro_jump from "resources/images/poro-jumping.jpg";


interface Props {
    onClose: () => any
}

export const LogRegTab: React.FC<Props> = ({onClose}) => {

    return (
        <Tab.Container transition={false} defaultActiveKey="login">
            <Nav variant="tabs" className="flex-row logReg--navbar">
                <Col xs={5} md={6}>
                    <Nav.Item className="logReg--navbar__firstTab">
                        <Nav.Link eventKey="login" aria-label="loginFormLoginTab">Anmeldung</Nav.Link>
                    </Nav.Item>
                </Col>
                <Col xs={6} md={6}>
                    <Nav.Item className="logReg--navbar__followingTab" >
                        <Nav.Link eventKey="register" aria-label="loginFormRegistrationTab">Registrierung</Nav.Link>
                    </Nav.Item>
                </Col>
                <Nav.Item className="logReg--closingButton">
                    <CloseButton variant="white" onClick={onClose} aria-label="loginFormClose"/>
                </Nav.Item>
            </Nav>
            <Tab.Content className="logReg--content">
                <Tab.Pane eventKey="login">
                    <div className="form-container">
                        <LoginForm onClose={onClose} />
                        <FillImage filePath={poro} parent="form-container" />
                    </div>
                </Tab.Pane>
                <Tab.Pane eventKey="register">
                    <div className="form-container">
                        <RegisterForm onClose={onClose} />
                        <FillImage filePath={poro_jump} parent="form-container" />
                    </div>
                </Tab.Pane>
            </Tab.Content>
        </Tab.Container>
    )
}

export default LogRegTab;

