import React from "react";

interface Feedback {
    form: string
    feedback: string[] | undefined
}

export const FormFeedback: React.FC<Feedback> = ({form, feedback}) => {

    const feedbackMessage = feedback?.map((errorMsg, key) =>
       <li key={key} >{errorMsg}</li>
    );

    return ( feedback?.length ? <ul className={form} aria-label="loginRegistrationError">{feedbackMessage}</ul> : null);
}

export default FormFeedback;

