import { useLogin } from "contexts/LoginProvider";
import React, {ReactElement} from "react";
import "./style/Introtext.scss";

interface TextProps {
    introHead: string,
    introText: string[],
    loginRelevance?: boolean,
    additionalElem?: ReactElement<any> | undefined,
    headerStyle?: string,
    bgStyle?: string,
}

export const IntroText: React.FC<TextProps> = ({introHead, introText = [], loginRelevance= true, additionalElem, bgStyle = "", headerStyle = "h1"}) => {
    const { isLoggedIn } = useLogin();

    if (loginRelevance && !isLoggedIn) {
        introHead = "Du bist nicht angemeldet."
        introText = ["Logge dich zuerst ein, um aktiv zu werden."];
    }

    if (introText.length > 0 || introHead !== "") {
        return <div className={"introText " + bgStyle}>
            {introHead !== "" ? <div className={headerStyle}>{introHead}</div> : "" }
            {introText.map((text, key) => {
                return (<p key={key}>
                    {text}
                </p>);
            })}
            {additionalElem ? additionalElem : ""}
        </div>;
    }

    return null;
}

export default IntroText;