import React from "react";
import {useTriviaGameData} from "contexts/TriviaDataProvider";
import AnswerField from "../AnswerField/AnswerField";
import {Col, Row} from "react-bootstrap";

interface Props {
    setGameStart: (newValue: boolean) => void
}

export const Question: React.FC<Props> = ({setGameStart}) => {
    const {  currentQuestion, selectAnswer, isFinished} = useTriviaGameData();
    
    if (isFinished) {
        setGameStart(false);
        return <div/>;
    }
    
    return (
    <div className="trivia--question">
        <h1>{currentQuestion.header}</h1>
        <Row className="question-answers g-3">
            {currentQuestion.answerOptions.map((answer, index) => {
                return (<Col md={3} key={index}>
                        <AnswerField answer={answer} selectAnswer={selectAnswer} />
                </Col>);
            })
            }
        </Row>
    </div>);
}
    
export default Question;

