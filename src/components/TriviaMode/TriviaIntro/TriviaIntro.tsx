import IntroText from "components/Introtext/Introtext";
import { useChampionDataContext } from "contexts/ChampionDataContext";
import { useTriviaGameData } from "contexts/TriviaDataProvider";
import React from "react";
import { Button } from "react-bootstrap";

interface Props {
    setGameStart: (param: boolean) => void
}

export const TriviaIntro: React.FC<Props> = ({setGameStart}) => {
    const { championDataMap, championNames} = useChampionDataContext();
    const { isFinished, getScore, createGame } = useTriviaGameData();
    const numberOfRounds = 8;

    return <div className="trivia--intro">
        { isFinished ? <IntroText introHead="Du hast dein Wissen unter Beweis gestellt" introText={[`Deine Punktzahl ist: ${getScore()}/${numberOfRounds}`]} bgStyle="success"/> : ""}
        <Button variant="outline-light trivia-gameStart" onClick={ () => {
            createGame(numberOfRounds, championDataMap, championNames);
            setGameStart(true);
        }}>Spiel Starten</Button>
    </div>;
}
    
export default TriviaIntro;