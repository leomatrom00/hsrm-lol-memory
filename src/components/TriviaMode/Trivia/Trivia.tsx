import IntroText from "components/Introtext/Introtext";
import { useLogin } from "contexts/LoginProvider";
import { TriviaDataProvider } from "contexts/TriviaDataProvider";
import React, { useState } from "react";
import Question from "../Question/Question";
import TriviaIntro from "../TriviaIntro/TriviaIntro";
import "../style/Trivia.scss"

export const Trivia: React.FC = () => {
    const { isLoggedIn } = useLogin();
    const [gameProcessing, setGameProcessing] = useState<boolean>(false);
    const header = "Wilkommen zum Trivia";
    const text = ["Sei schlauer als Heimerdinger und zeige Grips beim Runeterra Triviaquiz."];

    if (!isLoggedIn) {
        return <IntroText introHead={header} introText={text} />
    }

    return <div className="trivia">
        <TriviaDataProvider>
            <IntroText introHead={header} introText={text} />
            { gameProcessing ? <Question setGameStart={setGameProcessing}/>: <TriviaIntro setGameStart={setGameProcessing} />}
        </TriviaDataProvider>
    </div>;
}

export default Trivia;