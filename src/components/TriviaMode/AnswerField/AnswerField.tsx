import React from "react";
import { Button } from "react-bootstrap";
import {useTriviaGameData} from "../../../contexts/TriviaDataProvider";

interface AnswerProps {
    answer: string
    selectAnswer: (val: string) => void,
}

export const AnswerField: React.FC<AnswerProps> = ({answer, selectAnswer, }: AnswerProps) => {
    //const [colorState, setColorState] = useState<string>("");
    const { currentQuestion, blocked } = useTriviaGameData();

    /*if (blocked) {
        if (answer === currentQuestion.correctAnswer) {
            setColorState("success");
        } else {
            setColorState("danger");
        }
    }*/

    return (<Button variant="outline-light question-answerField" onClick={ () => {
        if(!blocked) {
            selectAnswer(answer);
        }
    }
    }>{answer}</Button>);
}
    
export default AnswerField;