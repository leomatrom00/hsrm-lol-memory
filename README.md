# HSRM LoL memory

Willkommen zu Runeterra Board Games. Dies ist eine auf React basierende Web-Applikation, welche im Rahmen des Moduls Web-Engineering erarbeitet worden ist.

## Installation

1. Führen Sie den Befehl "npm run develop" oder "yarn develop" in einem Git fähigen Terminal Ihrer Wahl im Projektverzeichnis root -> rbg-database aus, um die Strapi-Anwendung zu starten.
2. Nachdem Strapi gestartet wurde, erstellen Sie sich unter der URL "localhost:1337/admin" einen admin Account und melden Sie sich dort damit an.
3. Öffnen Sie danach Settings -> Users & Permissions Plugin -> Roles und editieren Sie den Eintrag zu "Authenticated".
4. Dort unter "User-Permissions" setzen Sie dann bei dem Punkt "USER" Häkchen zu "fineOne", "find", "update" und "me".
5. Führen Sie dann im Haputverzeichnis des Projekts den Befehl "npm install" aus, um alle Abhängigkeiten zu laden.
5. Danach führen Sie an gleicher Stelle den Befehl "npm run start" aus, um die Hauptanwendung zu bauen und anschließend zu starten.
6. Anschließend können Sie die Anwendung unter "localhost:3000/" erreichen und nutzen.

## Funktionen
Die Funktionen der Anwendung lassen sich wie folgt beschreiben. Nutzer, die über einen Account verfügen, haben die Möglichkeit aus 2 Spielmodi zu wählen. Beim Memory-Modus wird ein Nutzer dazu aufgefordert einen Schwierigkeitsgrad zu wählen. Anschließend startet das Spiel mit einer Anzahl an verdeckten Memory-Karten. 
Alternativ kann ein Nutzer den Trivia-Modus starten in welchem er Fragen gestellt bekommt zu den Helden bzw. Champions aus dem League of Legends Universum.

### Nützliche Links

- [Strapi documentation](https://docs.strapi.io) - Offizielle Strapi Dokumentation
