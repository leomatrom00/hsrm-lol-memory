import { mockData } from "../../src/resources/testData/championData";
import { DIFFICULTIES } from "../../src/components/MemoryMode/MemoryBoard/Difficulty"

const validUsername = "Testus Maximus";
const validUserMail = "test@test.de";
const validPassword = "T3st#8Letters";

declare global {
    namespace Cypress {
        interface Chainable {
        register(): void
        }
    }
}

Cypress.Commands.add('register', () => {
    cy.get('[aria-label="loginButton"]').should('be.visible');
    cy.get('[aria-label="loginButton"]').first().click();
    cy.get('[aria-label="loginForm"]').should('be.visible');
    cy.get('[aria-label="loginFormRegistrationTab"]').click();

    cy.get('[aria-label="registrationForm"]').should('be.visible');
    cy.get('[aria-label="registrationFormUsername"]').should('be.visible');
    cy.get('[aria-label="registrationFormMail"]').should('be.visible');
    cy.get('[aria-label="registrationFormPassword"]').should('be.visible');
    cy.get('[aria-label="registrationFormPasswordRepeat"]').should('be.visible');
    cy.get('[aria-label="registrationFormRegistrationButton"]').should('be.visible');

    cy.get('[aria-label="registrationFormUsername"]').type(validUsername);
    cy.get('[aria-label="registrationFormMail"]').type(validUserMail);
    cy.get('[aria-label="registrationFormPassword"]').type(validPassword);
    cy.get('[aria-label="registrationFormPasswordRepeat"]').type(validPassword);

    cy.get('[aria-label="registrationFormRegistrationButton"]').click();
    cy.get('[aria-label="loginRegistrationError"]').should('not.exist');

    cy.get('[aria-label="loginFormLoginTab"]').click();

    cy.get('[aria-label="loginUsername"]').type(validUsername);
    cy.get('[aria-label="loginPassword"]').type(validPassword);
    cy.get('[aria-label="loginFormButton"]').click();
});


describe('Memory behaviour', () => {

    beforeEach(() => {
        cy.intercept(
            {
                method: 'POST', // Route all GET requests
                url: '/api/auth/local/register', // that have a URL that matches '/users/*'
              },
              [] // and force the response to be: []
        ).as('register');
        cy.intercept(
            {
                method: 'GET', // Route all GET on League API
                url: 'https://ddragon.leagueoflegends.com/**', // that have a URL match for register
            },
            mockData // and force the response to be: predefined test json data
        ).as('fetch champion data');
        cy.visit("/");
    })

    it('should not be playable when user is not logged in', () => {
        cy.get('[aria-label="navigationItemmemory"]').should('be.visible');
        cy.get('[aria-label="navigationItemmemory"]').click();
        cy.get('[aria-label="memoryStartButton"]').should('not.exist');
        cy.get('[aria-label="memoryDifficultyDropdown"]').should('not.exist');
        cy.get('[aria-label="memoryDifficultyDropdownValue"]').should('not.exist');
    });

    it('should be playable and configurable for logged in users', () => {
        cy.intercept(
            {
                method: 'GET', // Route all GET on League API
                url: 'https://ddragon.leagueoflegends.com/**', // that have a URL match for register
            },
            mockData // and force the response to be: predefined test json data
        ).as('fetch champion data');

        cy.register();

        cy.get('[aria-label="navigationItemmemory"]').should('be.visible');
        cy.get('[aria-label="navigationItemmemory"]').click();
        cy.get('[aria-label="memoryStartButton"]').should('be.visible');
        cy.get('[aria-label="memorySettingsEasy"]').should('not.exist');
        cy.get('[aria-label="memoryDifficultyDropdown"]').should('be.visible');
        cy.get('[aria-label="memoryDifficultyDropdownValue"]').should('be.visible');
        cy.get('[aria-label="memoryDifficultyDropdownValue"]').should('have.text', "Schwierigkeit");

        cy.get('[aria-label="memoryDifficultyDropdown"]').click();
        cy.get('[aria-label="memorySettingsEasy"]').should('be.visible');
        cy.get('[aria-label="memorySettingsMedium"]').should('be.visible');
        cy.get('[aria-label="memorySettingsHard"]').should('be.visible');

        cy.get('[aria-label="memorySettingsEasy"]').click();
        cy.get('[aria-label="memoryDifficultyDropdownValue"]').should('have.text', "Einfach");

        cy.get('[aria-label="memoryStartButton"]').click();

        cy.get('[aria-label="memoryBoard"]').should('be.visible');
        cy.get('[aria-label="memoryCardback"]').should('have.length', DIFFICULTIES.EASY * 2);
        cy.get('[aria-label="memoryCardImage"]').should('not.exist');
        cy.get('[aria-label="memoryCardClickable"]').first().should('be.visible');
        cy.get('[aria-label="memoryCardClickable"]').first().click();
        cy.get('[aria-label="memoryCardImage"]').should('have.length', 1);
        cy.get('[aria-label="memoryCardClickable"]').eq(1).should('be.visible');
        cy.get('[aria-label="memoryCardClickable"]').eq(1).click();
        cy.get('[aria-label="memoryCardImage"]').should('have.length', 2);
        cy.wait(400).then(() => {
            cy.get('[aria-label="memoryCardImage"]').then((memoryImage1) => {
                const text = memoryImage1.text.toString();
                const text2 = cy.get('[aria-label="memoryCardImage"]').eq(1).then((memoryImage2) => memoryImage2.text).toString();
                if (text === text2) {
                    //should show both memory cards because there were the same champion
                    //cy.get('[aria-label="memoryCardImage"]').should('have.length', 2);
                } else {
                    //should remove images for both memory cards because there were different
                    //cy.get('[aria-label="memoryCardImage"]').should('not.exist');
                }
            });
        });
    });

});

