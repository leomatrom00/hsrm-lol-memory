import { mockData } from "../../src/resources/testData/championData";

const validUsername = "Testus Maximus";
const validUserMail = "test@test.de";
const validPassword = "T3st#8Letters";

describe('Login and Registration', () => {

  beforeEach(() => {
    cy.intercept(
      {
        method: 'POST', // Route all GET requests
        url: '/api/auth/local/register', // that have a URL that matches '/users/*'
      },
      [] // and force the response to be: []
    ).as('register');
    cy.intercept(
      {
        method: 'GET', // Route all GET on League API
        url: 'https://ddragon.leagueoflegends.com/**', // that have a URL match for register
      },
      mockData // and force the response to be: predefined test json data
    ).as('fetch champion data');
    cy.visit("/");
  });

  it('should open by click on login and close by click on X', () => {
    cy.get('[aria-label="loginForm"]').should('not.exist');
    cy.get('[aria-label="logoutButton"]').should('not.exist');
    cy.get('[aria-label="profileButton"]').should('not.exist');
    cy.get('[aria-label="loginButton"]').should('be.visible');
    cy.get('[aria-label="loginButton"]').first().click();
    cy.get('[aria-label="loginForm"]').should('be.visible');
    cy.get('[aria-label="loginFormClose"]').should('be.visible');
    cy.get('[aria-label="loginFormClose"]').click();
    cy.get('[aria-label="loginForm"]').should('not.exist');
  });

  it('should show error after invalid login', () => {
    cy.get('[aria-label="loginButton"]').should('be.visible');
    cy.get('[aria-label="loginButton"]').first().click();
    cy.get('[aria-label="loginForm"]').should('be.visible');
    cy.get('[aria-label="loginRegistrationError"]').should('not.exist');
    cy.get('[aria-label="loginUsername"]').type("Username that is not defined");
    cy.get('[aria-label="loginPassword"]').type("123456");
    cy.get('[aria-label="loginFormButton"]').click();
    cy.get('[aria-label="loginRegistrationError"]').should('be.visible');
  });

  it('should show let user register with valid data', () => {
    cy.get('[aria-label="loginButton"]').should('be.visible');
    cy.get('[aria-label="loginButton"]').first().click();
    cy.get('[aria-label="loginForm"]').should('be.visible');
    cy.get('[aria-label="loginFormRegistrationTab"]').click();

    cy.get('[aria-label="registrationForm"]').should('be.visible');
    cy.get('[aria-label="registrationFormUsername"]').should('be.visible');
    cy.get('[aria-label="registrationFormMail"]').should('be.visible');
    cy.get('[aria-label="registrationFormPassword"]').should('be.visible');
    cy.get('[aria-label="registrationFormPasswordRepeat"]').should('be.visible');
    cy.get('[aria-label="registrationFormRegistrationButton"]').should('be.visible');
    cy.get('[aria-label="registrationFormRegistrationButton"]').click();

    cy.get('[aria-label="registrationFormSuccess"]').should('not.exist');

    cy.get('[aria-label="registrationFormUsername"]').type(validUsername);
    cy.get('[aria-label="registrationFormMail"]').type(validUserMail);
    cy.get('[aria-label="registrationFormPassword"]').type(validPassword);
    cy.get('[aria-label="registrationFormPasswordRepeat"]').type(validPassword + " ");

    cy.get('[aria-label="registrationFormRegistrationButton"]').click();
    cy.get('[aria-label="loginRegistrationError"]').should('be.visible');

    cy.get('[aria-label="registrationFormPasswordRepeat"]').clear();
    cy.get('[aria-label="registrationFormPasswordRepeat"]').type(validPassword);

    cy.get('[aria-label="registrationFormRegistrationButton"]').click();
    cy.get('[aria-label="loginRegistrationError"]').should('not.exist');
    cy.get('[aria-label="registrationFormSuccess"]').should('be.visible');

    cy.get('[aria-label="loginFormLoginTab"]').click();

    cy.get('[aria-label="loginUsername"]').type(validUsername);
    cy.get('[aria-label="loginPassword"]').type(validPassword);
    cy.get('[aria-label="loginFormButton"]').click();
    cy.get('[aria-label="loginRegistrationError"]').should('not.exist');
  });

});

